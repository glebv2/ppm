import gi
import os

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class done(Gtk.Window):
    def __init__(self):
       super().__init__(title="done")
       box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=2)
       self.add(box)
       button1 = Gtk.Button(label="exit")
       label = Gtk.Label(label="done!")
       box.pack_start(label, True, True, 0)
       box.pack_start(button1, True, True, 0)
       button1.connect("clicked", exit)
    def exit():
       Gtk.main_quit()

class main(Gtk.Window):
    def __init__(self):
       super().__init__(title="ppm-gui")
       button1 = Gtk.Button(label="install")
       button2 = Gtk.Button(label="remove")
       button3 = Gtk.Button(label="exit")
       self.box = Gtk.Box(spacing=3)
       self.add(self.box)
       self.box.pack_start(button1, True, True, 0)
       self.box.pack_start(button2, True, True, 0)
       self.box.pack_start(button3, True, True, 0)
       button1.connect("clicked", self.install)
       button2.connect("clicked", self.remove)
       button3.connect("clicked", self.exit)
    def install(self,widget):
       os.system("python ppm.py -i")
       win = done()
       win.connect("destroy", Gtk.main_quit)
       win.show_all()
       Gtk.main()
    def remove(self,widget):
       os.system("python ppm.py -r")
       win = done()
       win.connect("destroy", Gtk.main_quit)
       win.show_all()
       Gtk.main()
    def exit(self,widget):
       Gtk.main_quit()


win = main()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
