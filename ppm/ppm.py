import sys
import os
import time
import sqlite3

if sys.argv == ['ppm.py', '-h']:
    print("Hi whis is ppm(program package manager)")
    print("My arguments")
    print("-i install -r remove -in info -w create work space -l list package")

if sys.argv == ['ppm.py', '-w']:
    os.system('mkdir ppmpackage')
    os.system('touch ppmpackage/info')
    os.system('touch ppmpackage/install')
    os.system('touch ppmpackage/remove')
    os.system('touch ppmpackage/db.py')
    os.system('touch ppmpackage/dbrm.py')

if sys.argv == ['ppm.py']:
   print("Arguments 0 -h = help")
   exit()

def remove_cache():
    os.system("rm -rdf ppmpackage")

if sys.argv == ['ppm.py', '-r']:
    os.system('unzip install.ppm.zip')
    time.sleep(1)
    os.system('bash ppmpackage/remove')
    remove_cache()

if sys.argv == ['ppm.py', '-i']:
    os.system("unzip install.ppm.zip")
    time.sleep(1)
    os.system("bash ppmpackage/install")
    remove_cache()


if sys.argv == ['ppm.py', '-in']:
    os.system("unzip install.ppm.zip")
    time.sleep(1)
    os.system("cat ppmpackage/info")
    remove_cache()

if sys.argv == ['ppm.py', '-l']:
    dbconn = sqlite3.connect('package.db')
    sql = dbconn.cursor()
    packages = sql.execute("SELECT name FROM sqlite_master WHERE type='table';")
    print(sql.fetchall())